const path = require('path');

module.exports = function override(config) {
  config.resolve = {
    ...config.resolve,
    alias: {
      ...config.alias,
      '@Core': path.resolve(__dirname, 'src/Core'),
      '@Domain': path.resolve(__dirname, 'src/Core'),
      '@Entities': path.resolve(__dirname, 'src/Core/Domain'),
      '@Infrastructure': path.resolve(__dirname, 'src/Core/Domain/Entities'),
      '@Store': path.resolve(__dirname, 'src/Core/Infrastructure'),
      '@Pages': path.resolve(__dirname, 'src/Views/Store'),
      '@Components': path.resolve(__dirname, 'src/Views/Pages'),
      '@Repositories': path.resolve(__dirname, 'src/Core/Infrastructure/Repositories')
    },
  };

  return config;
};