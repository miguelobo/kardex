import { calcStock } from "../Core/shared/Utils";
import { Movements } from '../Core/Domain/Entities/Movements';

interface IError {
    message: string
}
describe('Should try the calculate stock method', () => {
    it('Should try the stock calculation method when it is on sale.', async function () {
        const dataMovement = {
            id: "12",
            cant: 2,
            idProduct: "asd",
            name: "Product 1",
            type: "Venta"
        };
        const data = new Movements(dataMovement)
        const newStock =calcStock(5,data)
        expect(newStock).toBe(3)
    });
    it('You should try the inventory calculation method when it is sale and not product available.', async function () {
        const dataMovement = {
            id: "12",
            cant: 6,
            idProduct: "asd",
            name: "Product 1",
            type: "Venta"
        };
        const data = new Movements(dataMovement)
        expect.assertions(1);
        try {
            calcStock(5,data)
        } catch (e: IError| any) {
            expect(e.message).toBe("Producto No Disponible");
        }
        
    });

    
    it('Should try the stock calculation method when you buy.', async function () {
        const dataMovement = {
            id: "12",
            cant: 10,
            idProduct: "asd",
            name: "Product 1",
            type: "Compra"
        };
        const data = new Movements(dataMovement)
        const newStock =calcStock(5,data)
        expect(newStock).toBe(15)

    });
});
