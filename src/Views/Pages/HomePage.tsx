import { Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import { useAuth } from '../AuthContext';
import ProductList from '../Components/Product/ProductList';
import * as CartActions from '../Store/Actions/Cart';
import { useActions } from '../Store/Actions/index';

export function HomePage() {
	const cartActions: typeof CartActions = useActions(CartActions);
	const classes = useStyles();
	const { handleGreeting,greeting,currentUser } = useAuth();
   
	React.useEffect(() => {
		if(!!handleGreeting && !!currentUser && !!currentUser.uid){
			handleGreeting(currentUser.uid)
			cartActions.getCart(currentUser.uid)
		}
	},[])

	return (
		<div className={classes.root}>
			<Typography variant="h4" gutterBottom>
				Kardex
			</Typography>
			<Typography variant="subtitle2" gutterBottom>
				{greeting}
			</Typography>
		<ProductList />
		</div>
	);
}



const useStyles = makeStyles({
	root: {
		height: "100%",
		textAlign: "center",
		paddingTop: 20,
		paddingLeft: 15,
		paddingRight: 15,
	},

	centerContainer: {
		flex: 1,
		height: "90%",
		display: "flex",
		alignItems: "center",
		justifyContent: "center",
		flexDirection: "column",
	},

	button: {
		marginTop: 20,
	},
});
