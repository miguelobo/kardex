import { Button, Checkbox, CheckboxProps, CircularProgress, FormControlLabel, Grid, IconButton, InputAdornment, makeStyles, TextField, Typography, withStyles } from '@material-ui/core';
import { green } from '@material-ui/core/colors';
import { Visibility, VisibilityOff } from '@material-ui/icons';
import Alert from '@material-ui/lab/Alert';
import { Field, Form, Formik } from 'formik';
import React, { FC } from 'react';
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';
import { useAuth } from 'Views/AuthContext';
import { boolean, object, string } from 'yup';
import Background from '../Components/Common/Background';

const useStyles = makeStyles(() => ({
	item: {
		margin: '7px 0',
		'& .MuiFormControl-root': {
			margin: 0,
			'& .MuiInputBase-root ': {
				backgroundColor: 'white',
				borderRadius: 50,
				'& .MuiOutlinedInput-input': { padding: '5px 15px' },
			},
			'& .MuiFormHelperText-root': { marginTop: 0 },
		},
	},
	accept: {
		backgroundColor: '#193104',
		color: 'white',
		borderRadius: 25,
		padding: '3px 15px',
		boxShadow: 'none',
		marginBottom: 15,
		'&:hover': { backgroundColor: 'white', color: '#193104' },
	},
	button: { borderRadius: 25, padding: '3px 15px', boxShadow: 'none' },
	page: { height: '100vh' },

	login: {
		backgroundColor: 'rgba(255, 255, 255, 0.9)',
		position: 'relative',
		width: 450,
		height: '100%',
		padding: 30,
	},
	conten: { height: 'calc(100% - 85px)',marginTop:70 },
	footer: { '& b': { color: '#193104' } },
	imgLogo: {
		width: 197,
		height: 129,
		marginBottom: '-112.88px',
		position: 'relative',
		right: 102,
		top: '-7px',
	},
}));

const GreenCheckbox = withStyles({
	root: {
	  color: green[400],
	  '&$checked': {
		color: green[600],
	  },
	},
	checked: {},
  })((props: CheckboxProps) => <Checkbox color="default" {...props} />);

interface IInitialValues {
	email: string;
	password: string;
	name: string;

	isEmploye: boolean;
}

interface IProps {}
const routeImg = 'https://i.pinimg.com/originals/e2/0c/26/e20c260231bc119468aa13ef490d8de8.jpg';
const routeLogo = 'https://www.megaidea.net/wp-content/uploads/2021/09/Hulk-06.png';

export const SingUpPage: FC<IProps> = () => {
	const classes = useStyles();
	const { signup, logout } = useAuth();
	const history = useHistory();
	const [password, setPassword] = React.useState(true);
	const [loading, setLoading] = React.useState(false);
	const [error, setError] = React.useState('');
	const initialValues = { email: '', password: '', name: '', isEmploye: false  };
	const validations = object().shape({
		email: string().required('Campo requerido').email('Ej: example@example.com'),
		password: string().required('Campo requerido'),
		name: string().required('Campo requerido'),
		isEmploye: boolean().required('Campo requerido'),
	});
	const onSubmit = async (values: IInitialValues) => {
		try {
			logout();
			setError('');
			setLoading(true);
			if (!!signup) {
				await signup(values.email, values.password, values.name,values.isEmploye);
				history.push('/');
				setLoading(false);
			}
		} catch {
			setLoading(false);
			setError('Failed to register');
		}
	};
	const changePassword = () => setPassword(!password);

	return (
		<Grid className={classes.page} container alignItems="center" justify="flex-end">
			<Background imagedef={routeImg} />

			<Grid className={classes.login} container alignItems="stretch">
				<Grid container alignItems="center">
					<Grid item xs={6}></Grid>
					<Grid item container justify="flex-end" xs={6}>
						<img className={classes.imgLogo} alt="logo" src={routeLogo} />
					</Grid>
				</Grid>

				<Grid className={classes.conten} item container alignItems="center" xs={12}>
					<Grid container>
						{!!error && <Alert severity="error">{error}</Alert>}
						<Formik initialValues={initialValues} validationSchema={validations} onSubmit={onSubmit}>
							{({ errors, touched }) => (
								<Form>
									<Typography variant="h6">
										<b>{'Registrar'}</b>
									</Typography>

									<Grid container>
										<Grid className={classes.item} item xs={12}>
											<Field
												as={TextField}
												name="name"
												variant="outlined"
												placeholder={'Nombres'}
												size="small"
												fullWidth
												autoFocus
												error={errors.name && touched.name}
												helperText={touched.name && errors.name}
											/>
										</Grid>
										
										<Grid className={classes.item} item xs={12}>
											<Field
												as={TextField}
												name="email"
												variant="outlined"
												placeholder={'Correo electrónico'}
												size="small"
												fullWidth
												autoFocus
												error={errors.email && touched.email}
												helperText={touched.email && errors.email}
											/>
										</Grid>
										<Grid className={classes.item} item xs={12}>
											<Field
												as={TextField}
												name="password"
												variant="outlined"
												placeholder={'Contraseña'}
												type={!!password ? 'password' : 'text'}
												size="small"
												fullWidth
												error={errors.password && touched.password}
												helperText={touched.password && errors.password}
												InputProps={{
													endAdornment: (
														<InputAdornment position="end">
															<IconButton onClick={changePassword} size="small">
																{!!password ? <Visibility /> : <VisibilityOff />}
															</IconButton>
														</InputAdornment>
													),
												}}
											/>
										</Grid>
										<Grid className={classes.item} item xs={12}>
										<FormControlLabel
										control={
											<Field
												as={GreenCheckbox}
												name="isEmploye"
												variant="outlined"
												size="small"
												autoFocus
												error={errors.isEmploye && touched.isEmploye}
											/>
										}
											label="¿Es Empleado?"
										/>
											
										</Grid>
										<Grid className={classes.item} item xs={12}>
											<Button
												className={classes.accept}
												variant="contained"
												fullWidth
												type="submit"
												disabled={loading}
											>
												{!loading ? 'Registrar' : <CircularProgress size={25} />}
											</Button>
											<Button
												className={classes.accept}
												variant="contained"
												fullWidth
												component={Link}
												to="/login"
											>
												Login
											</Button>
										</Grid>
									</Grid>
								</Form>
							)}
						</Formik>
					</Grid>
				</Grid>
			</Grid>
		</Grid>
	);
};

export default SingUpPage;
