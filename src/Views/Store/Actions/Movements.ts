import { MovementsAction, MovementsActions } from '../Types/index';
import { MovementsRepositoryImpl } from '@Core/Infrastructure/Repositories/Movements/MovementsRepositoryImpl';
import { MovementsService } from '@Core/Usecases/MovementsService';
import { MovementsMapper } from '@Core/Mappers/MovementsMapper';
import { ProductMapper } from '@Core/Mappers/ProductMapper';
import { ProductRepositoryImpl } from '@Core/Infrastructure/Repositories/Product/ProductRepositoryImpl';

interface Imovement {
	id: string;
	type: string;
	name: string;
	cant: number;
	idProduct: string;
}

const mapperMovements= new MovementsMapper()
const movementRepo = new MovementsRepositoryImpl(mapperMovements);
const mapperProduct= new ProductMapper()
const productRepository = new ProductRepositoryImpl(mapperProduct);
const movementService = new MovementsService(movementRepo,productRepository);

export const addMovement = (movement: Imovement): Promise<MovementsAction> => {
	// @ts-ignore
	return async function saveNewMovementThunk(dispatch) {
		const movementInstance = mapperMovements.DTOtoDomain({...movement})
		await movementService.addMovements(movementInstance);
		const ListMovementss = await movementService.getMovements();
		dispatch({ type: MovementsActions.ADD_MOVEMENTS, payload: ListMovementss });
	};
};

export const getMovements = (): Promise<MovementsAction> => {
	// @ts-ignore
	return async function getsMovementThunk(dispatch) {
		const ListMovementss = await movementService.getMovements();
		dispatch({ type: MovementsActions.GETS_MOVEMENTSS, payload: ListMovementss });
	};
}
