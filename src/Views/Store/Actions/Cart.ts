import { CartAction, CartActions } from '../Types/index';
import { Product } from '@Core/Domain/Entities/Product';
import { CartMapper } from '@Core/Mappers/CartMapper';
import CartItem from '@Core/Domain/Entities/CartItem';
import { ItemCartMapper } from '@Core/Mappers/ItemCartMapper';
import { CartService } from '@Core/Usecases/CartService';
import { CartRepositoryImpl } from '@Core/Infrastructure/Repositories/Cart/CartRepositoryImpl';
import { MovementsRepositoryImpl } from '@Core/Infrastructure/Repositories/Movements/MovementsRepositoryImpl';
import { MovementsMapper } from '@Core/Mappers/MovementsMapper';
import { MovementsService } from '@Core/Usecases/MovementsService';
import { ProductMapper } from '../../../Core/Mappers/ProductMapper';
import { ProductRepositoryImpl } from '../../../Core/Infrastructure/Repositories/Product/ProductRepositoryImpl';
import { toast } from 'react-toastify';
import { getProducts } from './Product';

  

const cartMapper = new CartMapper();
const cartRepo = new CartRepositoryImpl(cartMapper);
const movementMapper = new MovementsMapper();
const movementsRepo = new MovementsRepositoryImpl(movementMapper);
const mapperProducts= new ProductMapper()
const productRepository = new ProductRepositoryImpl(mapperProducts);
const movementService = new MovementsService(movementsRepo,productRepository)
const cartService = new CartService(cartRepo,movementService);

export function changeDrawerCart(isOpen: boolean): CartAction {
	return {
		type: CartActions.CHANGE_DRAWER_CAR,
		payload: isOpen
	};
}

export function addProductToCart(uid: string,product: Product): CartAction {
	// @ts-ignore
	return async function getCartThunk(dispatch) {
		const carItem: CartItem = new ItemCartMapper().DTOtoDomain(product)
		
		const resultListProducts =await cartService.addProductToCart(uid, carItem)
		dispatch({ type: CartActions.GET_LIST_CAR, payload: resultListProducts});
	};
}

export function getCart(uid: string): CartAction {
	// @ts-ignore
	return async function getCartThunk(dispatch) {
		const resultListProducts =await cartService.getCart(uid)
		dispatch({ type: CartActions.GET_LIST_CAR, payload: resultListProducts});
	};
}

export function removeProductToCart(uid: string,idCartItem: string): CartAction {
	// @ts-ignore
	return async function removeProductThunk(dispatch) {
		const resultListProducts =await cartService.removeProduct(uid, idCartItem)
		dispatch({ type: CartActions.GET_LIST_CAR, payload: resultListProducts});
	};
}

export function updateQuantityToProductToCart(uid: string,idCartItem: string,quantity: number): CartAction {
	// @ts-ignore
	return async function updateQuantityThunk(dispatch) {
		const resultListProducts =await cartService.editQuantityOfCartItem(uid, idCartItem,quantity)
		dispatch({ type: CartActions.GET_LIST_CAR, payload: resultListProducts});
	};
}

export function buyToCart(uid: string): CartAction {
	// @ts-ignore
	return async function buyToCartThunk(dispatch, getState) {
		try {
			const cartCurrent = getState().cart.cart
			const resultListProducts =await cartService.buyProducts(uid,cartCurrent)
			dispatch({ type: CartActions.GET_LIST_CAR, payload: resultListProducts});
			dispatch(getProducts())
			toast.success("Compra realizada! ", {position: toast.POSITION.TOP_LEFT});
		} catch (error: any) {
			if(!!error && !!error.message){
				toast.error(error.message, {position: toast.POSITION.TOP_LEFT});
				console.log(error.message)
			} 
				
		}
	};
}