import { ProductAction, ProductActions } from '../Types/index';
import { ProductRepositoryImpl } from '@Core/Infrastructure/Repositories/Product/ProductRepositoryImpl';
import { ProductsService } from '@Core/Usecases/ProductsService';
import { ProductMapper } from '../../../Core/Mappers/ProductMapper';

const mapperProducts= new ProductMapper()
const productRepository = new ProductRepositoryImpl(mapperProducts);
const productService = new ProductsService(productRepository);
interface Iproduct {
	id: string;
	name: string;
	description: string;
	price: number;
	code: string;
}

export const addProduct = (product: Iproduct): Promise<ProductAction> => {
	// @ts-ignore
	return async function saveNewProuctThunk(dispatch) {
		const productInstance = mapperProducts.DTOtoDomain({...product,stock: 0})
		await productService.addProduct(productInstance);
		const ListProducts = await productService.getProducts();
		dispatch({ type: ProductActions.ADD_PRODUCT, payload: ListProducts });
	};
};

export const getProducts = (): Promise<ProductAction> => {
	// @ts-ignore
	return async function getProductsThunk(dispatch) {
		const ListProducts = await productService.getProducts();
		dispatch({ type: ProductActions.GETS_PRODUCTS, payload: ListProducts });
	};
}
