import Cart from "@Core/Domain/Entities/Cart";

export enum CartActions {
  CHANGE_DRAWER_CAR = "CHANGE_DRAWER_CAR",
  GET_LIST_CAR = "GET_LIST_CAR",
}

interface CartActionType<T, P> {
  type: T;
  payload: P;
}

export type CartAction =
  | CartActionType<typeof CartActions.CHANGE_DRAWER_CAR, boolean>
  | CartActionType<typeof CartActions.GET_LIST_CAR, Cart>
;
