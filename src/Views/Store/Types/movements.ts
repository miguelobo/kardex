import { Movements } from "@Core/Domain/Entities/Movements";

export enum MovementsActions {
  ADD_MOVEMENTS = "ADD_MOVEMENTS",
  GETS_MOVEMENTSS = "GETS_MOVEMENTSS",
}

interface MovementsActionType<T, P> {
  type: T;
  payload: P;
}

export type MovementsAction =
  | MovementsActionType<typeof MovementsActions.ADD_MOVEMENTS, Movements[]>
  | MovementsActionType<typeof MovementsActions.GETS_MOVEMENTSS, Movements[]>
;
