import { Product } from "@Core/Domain/Entities/Product";



export enum ProductActions {
  ADD_PRODUCT = "ADD_PRODUCT",
  DELETE_PRODUCT = "DELETE_PRODUCT",
  GET_PRODUCT = "GET_PRODUCT",
  GETS_PRODUCTS = "GETS_PRODUCTS",
}

interface ProductActionType<T, P> {
  type: T;
  payload: P;
}

export type ProductAction =
  | ProductActionType<typeof ProductActions.ADD_PRODUCT, Product[]>
  | ProductActionType<typeof ProductActions.GET_PRODUCT, Product>
  | ProductActionType<typeof ProductActions.GETS_PRODUCTS, Product[]>
  | ProductActionType<typeof ProductActions.DELETE_PRODUCT, number>
;
