import Cart from '@Core/Domain/Entities/Cart';
import { ConfigActions } from '../Types/config';
import {  CartAction, CartActions } from '../Types/index';
import createReducer from './createReducer';


export interface IStateCart {
	isOpen: boolean
	cart: Cart
}
const initialState: IStateCart = {
	isOpen:false,
	cart: new Cart([])
}
export const cart = createReducer<IStateCart>(initialState, {
	[CartActions.CHANGE_DRAWER_CAR](state: IStateCart, action: CartAction) {
		return {...state, isOpen: action.payload}
	},
	[CartActions.GET_LIST_CAR](state: IStateCart, action: CartAction) {
		return {...state, cart: action.payload}
	},
	[ConfigActions.PURGE_STATE](state: IStateCart, action: CartAction) {
		return {...state};
	},
});
