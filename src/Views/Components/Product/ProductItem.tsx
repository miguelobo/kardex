import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Product } from '../../../Core/Domain/Entities/Product';
import { Grid, Card, CardMedia, CardContent, Typography, CardActions, Button, Chip } from '@material-ui/core';
import { CurrencyConversion } from '../../../Core/shared/Utils';
import * as CartActions from "../../Store/Actions/Cart";
import { useActions } from '../../Store/Actions/index';
import { useAuth } from '../../AuthContext';

const useStyles = makeStyles((theme) => ({
	card: {
		height: '100%',
		display: 'flex',
		flexDirection: 'column',
	},
	cardMedia: {
		backgroundSize: 'auto 100%',
		paddingTop: '100%', // 16:9,
		margin: theme.spacing(1),
	},
	cardContent: {
		flexGrow: 1,
	},
	cardActions: {
		justifyContent: 'center',
	},
	productTitle: {
		overflow: 'hidden',
		textOverflow: 'ellipsis',
		height: 30,
	},
	productPrice: {
		textAlign: 'center',
	},
}));

interface ProductListProps {
	product: Product;
}

const ProductItem: React.FC<ProductListProps> = ({ product }) => {
	const classes = useStyles();
	const cartActions: typeof CartActions  = useActions(CartActions);
	const { currentUser } = useAuth();

	return (
		<Grid item xs={6} sm={4} md={3} lg={2}>
			<Card className={classes.card}>
				<CardMedia
					className={classes.cardMedia}
					image="http://x.annihil.us/u/prod/marvel/i/mg/3/40/4bb4680432f73/standard_fantastic.jpg"
					title="Image title"
				/>
				<CardContent className={classes.cardContent}>
					<Typography className={classes.productTitle}  variant="h6">
						{product.name}
					</Typography>
					<Typography variant="body1" className={classes.productPrice}>
						Precio: {CurrencyConversion(`${product.price}`, '$', 2)}
					</Typography>
                    <Typography variant="caption" className={classes.productPrice}>
						Disponibles: {product.stock}
					</Typography>
				</CardContent>
				<CardActions className={classes.cardActions}>
					{product.stock === 0 ? (
						<Chip size="medium" label="No Disponible" color="secondary" />
					) : (
						<Button size="small" color="primary" onClick={() => cartActions.addProductToCart(`${currentUser?.uid}`,product)}>
							Agregar
						</Button>
					)}
				</CardActions>
			</Card>
		</Grid>
	);
};

export default ProductItem;
