import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Container, Box, Typography } from '@material-ui/core';
import ProductItem from './ProductItem';
import { useSelector, useDispatch } from 'react-redux';
import { useActions } from '../../Store/Actions';
import * as ProductActions from '../../Store/Actions/Product';
import { RootState } from '../../Store/Reducers/index';

const useStyles = makeStyles((theme) => ({
	titleContainer: {
		marginBottom: theme.spacing(4),
	},
	cardGrid: {
		paddingTop: theme.spacing(4),
		paddingBottom: theme.spacing(4),
	},
	infoContainer: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
		height: '100vh',
	},
}));

const ProductList: React.FC = () => {
	const classes = useStyles();
	const dispatch = useDispatch();
	const ListProducts = useSelector((state: RootState) => state.productList.ListProducts);
	const productActions = useActions(ProductActions);

	React.useEffect(() => {
		dispatch(productActions.getProducts);
	}, [dispatch]);

	return (
		<Container className={classes.cardGrid} maxWidth="xl">
			<Box className={classes.titleContainer}>
				<Typography display="inline" variant="h6" component="h2">
					{'Listado de productos'}
				</Typography>
			</Box>
			<Grid container spacing={2}>
				{ListProducts.map((product, index) => (
					<ProductItem product={product} key={index} />
				))}
			</Grid>
		</Container>
	);
};

export default ProductList;
