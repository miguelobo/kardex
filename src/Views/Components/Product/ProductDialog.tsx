// prettier-ignore
import { Dialog } from "@material-ui/core";
import * as React from "react";
import { useActions } from "../../Store/Actions";
import * as ProductActions from "../../Store/Actions/Product";
import FormProduct from "./FormProduct";
import * as uuid from "uuid"

interface Props {
	open: boolean;
	onClose: () => void;
}
interface IInitialValues {
	name: string;
	description: string;
	price: number;
	code: string;
}

export function ProductDialog(props: Props) {
	const { open, onClose } = props;
	const productActions = useActions(ProductActions);

	const handleClose = (data: IInitialValues) => {
		onClose();
		productActions.addProduct({
			id: uuid.v4(),
			name: data.name,
			description: data.description,
			price: data.price,
			code: data.code
		});
		
	};


	return (
		<Dialog open={open} onClose={onClose}>
			<FormProduct handleClose={handleClose} />
		</Dialog>
	);
}
