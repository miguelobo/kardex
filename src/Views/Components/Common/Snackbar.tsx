// prettier-ignore
import { makeStyles, Snackbar as MuiSnackbar } from '@material-ui/core';
import  Alert  from "@material-ui/lab/Alert";
import * as React from "react";
import { useSelector } from "react-redux";
import { useActions } from "../../Store/Actions";
import * as SnackbarEventActions from "../../Store/Actions/snackbarEvent";
import { SnackbarEvent } from "../../Store/Types/snackbarEvent";
import { RootState } from "../../Store/Reducers";

export function Snackbar() {
	const classes = useStyles();
	const snackbarEvents: SnackbarEvent[] = useSelector(
		(state: RootState) => state.snackbarEvents
	);
	const snackbarEventActions: typeof SnackbarEventActions = useActions(
		SnackbarEventActions
	);
	const [currentEvent, setCurrentEvent] = React.useState(
		snackbarEvents.length > 0 ? snackbarEvents[0] : undefined
	);

	React.useEffect(() => {
		setCurrentEvent(
			snackbarEvents.length > 0 ? snackbarEvents[0] : undefined
		);
	}, [snackbarEvents]);

	const onClose = (
		event: React.SyntheticEvent | React.MouseEvent,
		reason?: string
	) => {
		if (reason === "clickaway") {
			return;
		}
		if (currentEvent) {
			snackbarEventActions.deleteSnackbarEvent(currentEvent);
		}
	};

	if (currentEvent) {
		return (
			<MuiSnackbar
				open={!!currentEvent}
				autoHideDuration={
					currentEvent.severity === "info" ? 2000 : 6000
				}
				onClose={onClose}
				anchorOrigin={{ horizontal: "center", vertical: "top" }}
				transitionDuration={500}
				className={classes.root}
			>
			<Alert
					onClose={onClose}
					severity={currentEvent.severity}
					variant={
						currentEvent.severity === "info" ? "standard" : "filled"
					}
					>
					Hulk Store
				</Alert> 

			</MuiSnackbar>
		);
	} else {
		return <></>;
	}
}

const useStyles = makeStyles({
	root: {
		zIndex: 99999999,
	},
});
