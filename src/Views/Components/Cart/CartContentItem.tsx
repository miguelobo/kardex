import React, { Key } from 'react';
import { makeStyles, Theme } from '@material-ui/core/styles';
import {
	ListItem,
	ListItemText,
	ListItemSecondaryAction,
	IconButton,
	TextField,
	Paper,
	Box,
	Typography,
} from '@material-ui/core';
import RemoveIcon from '@material-ui/icons/Clear';
import { CurrencyConversion } from '@Core/shared/Utils';
import * as CartActions from '../../Store/Actions/Cart';
import { useActions } from '../../Store/Actions/index';
import { useAuth } from '../../AuthContext';
import CartItem from '@Core/Domain/Entities/CartItem';

const useStyles = makeStyles((theme: Theme) => ({
	itemContainer: {
		margin: theme.spacing(1),
	},
	itemImage: {
		padding: theme.spacing(0, 1),
		backgroundSize: 'auto 100%',
	},
	secondContainer: {
		display: 'flex',
		alignItems: 'center',
		padding: theme.spacing(1, 0),
		justifyContent: 'space-around',
	},
	quantityField: {
		marginTop: theme.spacing(1),
		width: 60,
	},
}));

interface CartProps {
	key: Key;
	cartItem: CartItem;
}

const CartContentItem: React.FC<CartProps> = ({ key, cartItem }) => {
	const classes = useStyles();
	const cartActions: typeof CartActions = useActions(CartActions);
	const { currentUser } = useAuth();
	return (
		<React.Fragment>
			<Paper className={classes.itemContainer}>
				<ListItem key={key}>
					<img width={80} className={classes.itemImage} src={cartItem.image} alt={cartItem.title} />
					<ListItemText
						primary={cartItem.title}
						secondary={
							<Box flexDirection="row" className={classes.secondContainer}>
								<TextField
									id="standard-number"
									label="Cantidad"
									type="number"
									className={classes.quantityField}
									InputLabelProps={{
										shrink: true,
									}}
                                    InputProps={{ inputProps: { min: 1 } }}
									margin="none"
									value={cartItem.quantity}
									onChange={(event) =>{
                                        cartActions.updateQuantityToProductToCart(`${currentUser?.uid}`, cartItem.id, parseInt(event.target.value))
									} }
								/>
								<Typography variant="body1">
									{CurrencyConversion(`${cartItem.price}`, '$', 2)}
								</Typography>
							</Box>
						}
					/>
					<ListItemSecondaryAction>
						<IconButton edge="end" aria-label="delete">
							<RemoveIcon onClick={() =>cartActions.removeProductToCart(`${currentUser?.uid}`, cartItem.id)} />
						</IconButton>
					</ListItemSecondaryAction>
				</ListItem>
			</Paper>
		</React.Fragment>
	);
};

export default CartContentItem;
