import React from "react";
import {makeStyles, Theme} from "@material-ui/core/styles";
import {Drawer, IconButton, Divider, Typography, Box} from "@material-ui/core";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import CartContent from "./CartContent";
import { useSelector } from "react-redux";
import { RootState } from '../../Store/Reducers/index';
import * as CartActions from "../../Store/Actions/Cart";
import { useActions } from '../../Store/Actions/index';
import CloseIcon from '@material-ui/icons/Close';
const drawerWidth = 350;
const useStyles = makeStyles((theme: Theme) => ({
    drawer: {
        width: drawerWidth,
    },
    drawerPaper: {
        width: drawerWidth,
    },
    drawerHeader: {
        display: "flex",
        alignItems: "center",
        padding: theme.spacing(1, 0),
        justifyContent: "flex-start",
    },
    drawerTitleContainer: {
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
    },
    drawerTitleIcon: {
        marginRight: theme.spacing(1),
    },
}));

const CartDrawer: React.FC = () => {
    const classes = useStyles();
    const cartActions = useActions(CartActions);
    const isOpen = useSelector((state: RootState) => state.cart.isOpen);

    return (
      
        <Drawer
            anchor="right"
            open={isOpen}
            className={classes.drawer}
            classes={{
                paper: classes.drawerPaper,
            }}
        >
            <Box className={classes.drawerHeader}>
                <IconButton onClick={() => cartActions.changeDrawerCart(false)}>
                    <CloseIcon />
                </IconButton>
                <Box className={classes.drawerTitleContainer}>
                    <ShoppingCartIcon className={classes.drawerTitleIcon} />
                    <Typography variant="h6" component="h2">
                        Cart
                    </Typography>
                </Box>
            </Box>
            <Divider />
        <CartContent /> 
        </Drawer>
    );
};

export default CartDrawer;
