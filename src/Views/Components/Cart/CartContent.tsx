import React from 'react';
import { makeStyles, Theme } from '@material-ui/core/styles';
import { List, Divider, Box, Typography, Grid, Button } from '@material-ui/core';
import CartContentItem from './CartContentItem';
import { useSelector } from 'react-redux';
import { RootState } from '../../Store/Reducers/index';
import { CurrencyConversion } from '@Core/shared/Utils';
import * as CartActions from '../../Store/Actions/Cart';
import { useActions } from '../../Store/Actions/index';
import { useAuth } from '../../AuthContext';

const useStyles = makeStyles((theme: Theme) => ({
	totalPriceContainer: {
		display: 'flex',
		alignItems: 'center',
		padding: theme.spacing(1, 0),
		justifyContent: 'space-around',
	},
	itemsContainer: {
		display: 'flex',
		padding: theme.spacing(1, 0),
		justifyContent: 'space-around',
		minHeight: 380,
		width: 333,
		textAlign: 'center',
		texTransform: 'capitalize',
	},
	itemsList: {
		overflow: 'scroll',
	},
	infoContainer: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
		height: '100vh',
	},buttonContainer: {
		display: "flex",
		margin: 18,
		justifyContent: "flex-end",
	},
	button: {
		marginBottom: 15,
		backgroundColor: '#193104 !important',
		color: 'white',
		borderRadius: 25,
		padding: '3px 15px',
		boxShadow: 'none',
		'&:hover': { backgroundColor: 'white !important', color: '#193104 !important' }
	},
}));

const CartContent: React.FC = () => {
	const classes = useStyles();
	const cart = useSelector((state: RootState) => state.cart.cart);
	const cartActions: typeof CartActions = useActions(CartActions);
	const { currentUser } = useAuth();

	const cartItems = (items: any[]) => (
		<List className={classes.itemsList}>
			{items.map((item, index) => (
				<CartContentItem key={index} cartItem={item} />
			))}
		</List>
	);

	const emptyCartItems = () => (
		<React.Fragment>
			<Typography variant="h6" component="h2">
				No tienes productos :(
			</Typography>
		</React.Fragment>
	);

	return (
		<React.Fragment>
			<Box flexDirection="column" className={classes.itemsContainer}>
				{cart.items.length > 0 ? cartItems(cart.items) : emptyCartItems()}
			</Box>
			<Divider />
			<Box flexDirection="row" className={classes.totalPriceContainer}>
				<Typography variant="h6" component="h2">
					Precio total
				</Typography>
				<Typography variant="h6" component="h2">
					{CurrencyConversion(`${cart.totalPrice}`, '$', 2)}
				</Typography>
			</Box>

			<Grid item xs={12} md={12}>
				<div className={classes.buttonContainer}>
					<Button
					id="addBuy"
						className={classes.button}
						variant="contained"
						color="primary"
						onClick={()=>cartActions.buyToCart(`${currentUser?.uid}`)}
					>
						Comprar
					</Button>
				</div>
			</Grid>
		</React.Fragment>
	);
};

export default CartContent;
