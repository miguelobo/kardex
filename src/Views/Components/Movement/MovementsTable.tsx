// prettier-ignore
import { Paper, Table, TableBody, TableCell, TableHead, TableRow } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import { useSelector, useDispatch } from 'react-redux';
import { useActions } from "../../Store/Actions";
import * as MovementActions from "../../Store/Actions/Movements";

import { RootState } from "../../Store/Reducers/index";

export function MovementsTable() {
	const classes = useStyles();
 const dispatch = useDispatch() 
	const MovementsList = useSelector((state: RootState) => state.movements.ListMovements);
	const movementActions = useActions(MovementActions);

	React.useEffect(() => {
		dispatch(movementActions.getMovements);
	}, [dispatch]) 

	return (
		<Paper className={classes.paper}>
			<Table className={classes.table}>
				<TableHead>
					<TableRow>
						<TableCell padding="default">Tipo</TableCell>
						<TableCell padding="default">Nombre</TableCell>
						<TableCell padding="default">Cantidad</TableCell>
					</TableRow>
				</TableHead>
				<TableBody>
					{!!MovementsList && !!MovementsList.map && MovementsList.map((n,index) => {
						return (
							<TableRow
								key={index}
								hover
							>
								<TableCell padding="none">{(!!n && !!n?.type) ? n?.type : ""}</TableCell>
								<TableCell padding="none">{(!!n && !!n?.name) ? n?.name : ""}</TableCell>
								<TableCell padding="none">{(!!n && !!n?.cant) ? n?.cant : ""}</TableCell>
								
							</TableRow>
						);
					})}
				</TableBody>
			</Table>
		</Paper>
	);
}

const useStyles = makeStyles({
	paper: {
		minWidth: 260,
		overflowY: "scroll",
		height: "400px",
		display: "block",
		width: "100%",
		maxHeight: "64vh",
		padding: "15px"
	},
	table: {
		width: "100%",
	},
});
