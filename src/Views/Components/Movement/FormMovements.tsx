import {
	Button,
	CircularProgress,
	FormControl,
	FormHelperText,
	Grid,
	makeStyles,
	Select,
	TextField,
	Typography,
} from '@material-ui/core';
import { Field, Form, Formik } from 'formik';
import React, { FC } from 'react';
import { number, object, string } from 'yup';
import MenuItem from '@material-ui/core/MenuItem';
import { useDispatch, useSelector } from 'react-redux';
import { useActions } from '../../Store/Actions/index';
import * as ProductActions from '../../Store/Actions/Product';
import { RootState } from '@Store/Reducers';
import { Product } from '@Core/Domain/Entities/Product';

const useStyles = makeStyles(() => ({
	item: {
		margin: '7px 0',
		'& .MuiFormControl-root': {
			margin: 0,
			'& .MuiInputBase-root ': {
				backgroundColor: 'white',
				borderRadius: 50,
				'& .MuiOutlinedInput-input': { padding: '5px 15px' },
			},
			'& .MuiFormHelperText-root': { marginTop: 0 },
		},
	},
	accept: {
		backgroundColor: '#193104',
		color: 'white',
		borderRadius: 25,
		padding: '3px 15px',
		boxShadow: 'none',
		marginBottom: 15,
		'&:hover': { backgroundColor: 'white', color: '#193104' },
	},
	button: { borderRadius: 25, padding: '3px 15px', boxShadow: 'none' },
	page: { height: '100vh' },

	login: {
		backgroundColor: 'rgba(255, 255, 255, 0.9)',
		position: 'relative',
		width: 450,
		height: '100%',
		padding: 30,
	},
	conten: {
		height: 'calc(100% - 85px)',
		marginTop: 20,
		paddingTop: -1,
		paddingRight: 20,
		paddingBottom: 0,
		paddingLeft: 20,
	},
	footer: { '& b': { color: '#193104' } },
	imgLogo: {
		width: 197,
		height: 129,
		marginBottom: '-112.88px',
		position: 'relative',
		right: 102,
		top: '-42px',
	},
	formControl: {
		width: 191,
		marginLeft: "12px !important"
	},
}));

interface IInitialValues {
	type: string;
	name: string;
	cant: number;
	stock: number;
	idProduct: string;
}
const initialValues = {
	type: '',
	name: '',
	cant: 0,
	stock: 0,
	idProduct: '',
};

interface IProps {
	handleAdd: (values: IInitialValues) => void;
}

export const FormMovements: FC<IProps> = ({ handleAdd }) => {
	const classes = useStyles();
	const dispatch = useDispatch();
	const productList = useSelector((state: RootState) => state.productList.ListProducts);
	const productActions = useActions(ProductActions);

	const [loading, setLoading] = React.useState(false);
	
	const validations = object().shape({
		type: string().required('Campo requerido'),
		name: string().required('Campo requerido'),
		cant: number().required('Campo requerido').positive("Numero Positivo").integer('Campo Invalido').test(
			'global-ok',
        	"Producto no Disponible",
			 function (value,values) {
				 // @ts-ignore
				 if(values.parent.type === "Venta" && value > values.parent.stock){
					 return false
				 }
				return true;
			  }
		),
		idProduct: string().required('Campo requerido'),
	});

	const onSubmit = async (values: IInitialValues) => {
		setLoading(true);
		try {
			await handleAdd(values);
			setLoading(false);
		} catch (error) {
			setLoading(false);
		}
	};

	React.useEffect(() => {
		dispatch(productActions.getProducts);
	}, [dispatch]);

	return (
		<Grid className={classes.conten} item container alignItems="center" xs={12}>
			<Grid container>
				<Formik
					initialValues={initialValues}
					validationSchema={validations}
					validateOnMount={false}
					onSubmit={onSubmit}
				>
					{({ errors, touched, setFieldValue,values }) => (
						<Form>
							<Typography variant="h6">
								<b>{'Nuevo Movimiento'}</b>
							</Typography>

							<Grid container>
								<Grid className={classes.item} item xs={6}>
									<Typography variant="caption" color="initial">
										Producto
									</Typography>
									<FormControl className={classes.formControl}>
										<Select
											variant="outlined"
											displayEmpty
											fullWidth
											name="idProduct"
											
										>
											{!!productList &&
												!!productList.map &&
												productList.map((n: Product) => {
													return (
														<MenuItem onClick={()=>{
															setFieldValue('stock', n.stock)
															setFieldValue('name', n.name)
															setFieldValue('idProduct', n.id);
															}} key={n.id} value={n.id}>
															{n.name}
														</MenuItem>
													);
												})}
										</Select>
										{!!errors.type && !!touched.type && (
											<FormHelperText>{errors.type}</FormHelperText>
										)}
									</FormControl>
								</Grid>

								<Grid className={classes.item} item xs={6}>
									<Typography variant="caption" color="initial">
										Tipo
									</Typography>

									<FormControl className={classes.formControl}>
										<Select
											variant="outlined"
											displayEmpty
											fullWidth
											name="type"
											onChange={(e) => {
												setFieldValue('type', e.target.value);
											}}
										>
											<MenuItem value={'Compra'}>{'Compra'}</MenuItem>
											<MenuItem value={'Venta'}>{'Venta'}</MenuItem>
										</Select>
										{!!errors.type && !!touched.type && (
											<FormHelperText>{errors.type}</FormHelperText>
										)}
									</FormControl>
								</Grid>

								<Grid className={classes.item} item xs={12}>
									<Typography variant="caption" color="initial">
										Producto
									</Typography>

									<Field
										as={TextField}
										name="name"
										variant="outlined"
										placeholder={'Nombre'}
										size="small"
										fullWidth
										disabled={true}
										error={errors.name && touched.name}
										helperText={touched.name && errors.name}
									/>
								</Grid>

								<Grid className={classes.item} item xs={12}>
									<Typography variant="caption" color="initial">
										Cantidad
									</Typography>
									<Field
										as={TextField}
										type="number"
										name="cant"
										variant="outlined"
										placeholder={'Cantidad'}
										size="small"
										fullWidth
										error={errors.cant && touched.cant}
										helperText={touched.cant && errors.cant}
									/>
								</Grid>
								<Grid className={classes.item} item xs={12}>
									<Typography variant="subtitle2" color="initial">
										Stocks : {values.stock} 
									</Typography>
								</Grid>


								<Grid className={classes.item} item xs={12}>
									<Button
										className={classes.accept}
										type="submit"
										variant="contained"
										disabled={loading}
										fullWidth
									>
										{!loading ? 'Registrar' : <CircularProgress size={25} />}
									</Button>
								</Grid>
							</Grid>
						</Form>
					)}
				</Formik>
			</Grid>
		</Grid>
	);
};

export default FormMovements;
