// prettier-ignore
import { Dialog } from "@material-ui/core";
import * as React from "react";
import { useActions } from "../../Store/Actions";
import * as MovementsActions from "../../Store/Actions/Movements";
import * as uuid from "uuid"
import FormMovements from "./FormMovements";

interface Props {
	open: boolean;
	onClose: () => void;
}
interface IInitialValues {
	type: string;
	name: string;
	cant: number ;
	idProduct: string;
}

export function MovementsDialog(props: Props) {
	const { open, onClose } = props;
	const movementActions = useActions(MovementsActions);

	const handleAdd = (data: IInitialValues) => {
		onClose();
		movementActions.addMovement({
			id: uuid.v4(),
			type: data.type,
			name: data.name,
			cant: data.cant,
			idProduct: data.idProduct
		});
		
	};


	return (
		<Dialog open={open} onClose={onClose}>
			<FormMovements handleAdd={handleAdd} />
		</Dialog>
	);
}
