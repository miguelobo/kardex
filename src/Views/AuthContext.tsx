import React, { useContext, useState, useEffect } from "react"
import { RouterSwitch } from "react-typesafe-routes"
import { routerBlank } from "Router"
import { auth } from "../firebaseConfig"
import { UserRepositoryImpl } from '../Core/Infrastructure/Repositories/User/UserRepositoryImpl';
import { UserServiceImpl } from '../Core/Usecases/UserService';
import { UserMapper } from '../Core/Mappers/UserMapper';

type typeUser = {
    currentUser: firebase.default.User | null;
    value: firebase.default.User | null;
    login: (email:string, password:string)=> any | undefined;
    signup: any;
    logout: any;
    greeting: string
    handleGreeting: (uid: string)=> void

}
const stateDefault = {
  currentUser: null,
  value: null,
  login: undefined,
  signup: null,
  logout: null,
  greeting: "",
  handleGreeting: undefined
}
const AuthContextProv = React.createContext<Partial<typeUser>>(stateDefault)

export function useAuth() {
  return useContext(AuthContextProv)
}

export function AuthProvider({ children }: any) {
  const userMapper = new UserMapper()
  const userRepo = new UserRepositoryImpl(userMapper);
  const userService = new UserServiceImpl(userRepo);

  const [currentUser, setCurrentUser] = useState<firebase.default.User | undefined >()
  const [loading, setLoading] = useState(true)
  const [greeting, setGreeting] = useState<string>()

  function signup(email:string, password:string,name: string,isEmploye: boolean) {
     return userService.signup(email, password,name, isEmploye)
  }

  function login(email:string, password:string) {
    return userService.login(email, password)
  }

  function logout() {
    return userService.logout()
  }

 async function handleGreeting(uid: string) {
  setGreeting(await userService.greeting(uid))
 }
  
  
  useEffect(() => {
     const unsubscribe = auth.onAuthStateChanged(async (user): Promise<any> => {
       if(!!user){
         setCurrentUser(user)
         setLoading(false)
        }else{
          setCurrentUser(undefined)
         setLoading(true)
         setGreeting(undefined)
        }
    }) 

    return unsubscribe
  }, [])

  const value = {
    currentUser,
    login,
    signup,
    logout,
    greeting,
    handleGreeting
  }

  return (
    <AuthContextProv.Provider value={value} >
				{!currentUser && <RouterSwitch router={routerBlank}  />}
					
      {!loading && children}
    </AuthContextProv.Provider>
    
  )
}