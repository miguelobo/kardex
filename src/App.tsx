// prettier-ignore
import { AppBar, Badge, Grid, IconButton, Toolbar, Typography, useMediaQuery } from '@material-ui/core';
import { Theme, useTheme } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import { makeStyles } from '@material-ui/styles';
import * as React from 'react';
import { useSelector } from 'react-redux';
import { Router } from 'react-router-dom';
import { RouterSwitch } from 'react-typesafe-routes';
import { AuthProvider } from 'Views/AuthContext';
import { history } from './configureStore';
import { router } from './Router';
import CartDrawer from './Views/Components/Cart/CartDrawer';
import { Drawer } from './Views/Components/Common/Drawer';
import MenuLogout from './Views/Components/Common/Menu';
import { Snackbar } from './Views/Components/Common/Snackbar';
import { useActions } from './Views/Store/Actions';
import * as CartActions from './Views/Store/Actions/Cart';
import * as ConfigActions from './Views/Store/Actions/config';
import { RootState } from './Views/Store/Reducers';
import { withRoot } from './withRoot';
import { ToastContainer } from 'react-toastify';

function App() {
	const theme = useTheme();
	const isMobile = useMediaQuery(theme.breakpoints.down('sm'));
	const classes = useStyles();
	const cart = useSelector((state: RootState) => state.cart.cart);
	const drawerOpen: boolean = useSelector((state: RootState) => state.drawerOpen);
	const configActions: typeof ConfigActions = useActions(ConfigActions);
	const cartActions: typeof CartActions = useActions(CartActions);
	
	const handleDrawerToggle = () => {
		configActions.setDrawerOpen(!drawerOpen);
	};


	return (
		<Router history={history}>
			<AuthProvider>
				<div className={classes.root}>
					<div className={classes.appFrame}>
						<Snackbar />
						<AppBar className={classes.appBar}>
							<Toolbar>
								<Grid
									container
									spacing={1}
									direction="row"
									justify="flex-start"
									alignItems="flex-start"
									alignContent="stretch"
									wrap="nowrap"
								>
									<Grid container spacing={1}>
										<IconButton
											color="inherit"
											aria-label="open drawer"
											onClick={handleDrawerToggle}
											className={classes.navIconHide}
										>
											<MenuIcon />
										</IconButton>
										<Typography
											className={classes.textLogo}
											variant="h6"
											color="inherit"
											noWrap={isMobile}
										>
											Hulk Store
										</Typography>
									</Grid>
									<Grid
										container
										spacing={1}
										direction="row"
										justify="flex-start"
										alignItems="center"
										alignContent="stretch"
										wrap="nowrap"
										className={classes.containerOptions}
									>
										<IconButton color="inherit" className={classes.buttonIcon}>
											<Badge badgeContent={cart.items.length} color="secondary">
												<ShoppingCartIcon onClick={() => cartActions.changeDrawerCart(true)} />
											</Badge>
										</IconButton>
										<MenuLogout />
									</Grid>
								</Grid>
							</Toolbar>
						</AppBar>
						<Drawer />
						<CartDrawer />
						<div className={classes.content}>
							<RouterSwitch router={router} />
						</div>
					</div>
				</div>
				<ToastContainer />
			</AuthProvider>
		</Router>
	);
}

const useStyles = makeStyles((theme: Theme) => ({
	root: {
		width: '100%',
		height: '100%',
		zIndex: 1,
		overflow: 'hidden',
	},
	appFrame: {
		position: 'relative',
		display: 'flex',
		width: '100%',
		height: '100%',
	},
	appBar: {
		zIndex: theme.zIndex.drawer + 1,
		position: 'absolute',
		backgroundColor: '#193104 !important',
	},
	textLogo: {
		position: 'relative',
		top: 7,
	},
	navIconHide: {
		backgroundColor: '#193104 !important',
		[theme.breakpoints.up('md')]: {
			display: 'none',
		},
	},
	content: {
		backgroundColor: theme.palette.background.default,
		width: '100%',
		height: 'calc(100% - 56px)',
		marginTop: 56,
		[theme.breakpoints.up('sm')]: {
			height: 'calc(100% - 64px)',
			marginTop: 64,
		},
	},
	containerOptions: {
		[theme.breakpoints.up('sm')]: {
			width: '120px !important',
			justifyContent: 'center !important',
			alignItems: 'center !important',
		},
		[theme.breakpoints.up('md')]: {
			width: '120px !important',
			justifyContent: 'center !important',
			alignItems: 'center !important',
		},
	},
	buttonIcon: {
		top: '2px !important',
	},
}));

export default withRoot(App);
