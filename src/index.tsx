
import * as React from "react";
import * as ReactDOM from "react-dom";
import { ReduxRoot } from "./ReduxRoot";
import 'react-toastify/dist/ReactToastify.css';

const rootEl = document.getElementById("root");
ReactDOM.render(<ReduxRoot />, rootEl);