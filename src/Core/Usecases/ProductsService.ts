import { Product } from '@Entities/Product';
import { ProductRepository } from '@Repositories/Product/ProductsRepository';

interface IProductsService {
    getProducts(): Promise<Product[]> 
    addProduct(data: Product): Promise<Product | undefined>
}

export class ProductsService implements IProductsService {
    productRepository: ProductRepository
    constructor(pr: ProductRepository) {
        this.productRepository = pr
    }

    async getProducts (): Promise<Product[]> {
        return this.productRepository.getProducts()
    }

    async addProduct (data: Product): Promise<Product | undefined> {
        return this.productRepository.addProduct(data)
    }
}