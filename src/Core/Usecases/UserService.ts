import { UserRepository } from "@Repositories/User/UserRepository";
import { User } from '../Domain/Entities/User';
import { Employes } from '../Domain/Entities/Employes';
export interface UserService {
  signup(email:string, password:string,name: string,isEmploye: boolean): Promise<void>
  logout(): Promise<void>
  login(email:string, password:string): Promise<void>
  greeting(uid:string):  Promise<string>
}

export class UserServiceImpl implements UserService {
  userRepo: UserRepository;

  constructor(ir: UserRepository) {
    this.userRepo = ir;
  }

  async signup(email:string, password:string,name: string,isEmploye: boolean): Promise<void> {
    return this.userRepo.signup(email,password,name, isEmploye);
  }

  async login(email:string, password:string): Promise<void> {
    return this.userRepo.login(email,password);
  }

  async logout(): Promise<void> {
    return this.userRepo.logout();
  }

  async greeting(uid:string): Promise<string> {
    const result = await this.userRepo.greeting(uid);
    if(result.isLeft()){
      return result.value
    }
    const user: User | Employes =result.value
    return user.greeting()
  }
  
}
