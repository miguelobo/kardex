import { Movements } from '@Entities/Movements';
import { MovementsRepository } from '@Repositories/Movements/MovementsRepository';
import { ProductRepository } from '../Infrastructure/Repositories/Product/ProductsRepository';
import { calcStock } from '../shared/Utils';

interface IMovementsService {
    getMovements(): Promise<Movements[]> 
    addMovements(data: Movements): Promise<Movements | undefined>
}

export class MovementsService implements IMovementsService {
    movementRepository: MovementsRepository
    productRepository: ProductRepository
    constructor(mr: MovementsRepository,pr: ProductRepository) {
        this.movementRepository = mr
        this.productRepository = pr
    }

    async getMovements (): Promise<Movements[]> {
        return this.movementRepository.getMovements()
    }

    async addMovements (data: Movements): Promise<Movements | undefined> {
        const responseValues=await this.productRepository.getProductById(data.idProduct)
        const newStock = calcStock(responseValues.stock, data);
        await this.productRepository.updateProductById(data.idProduct,newStock)
        return this.movementRepository.addMovement(data)
    }
}