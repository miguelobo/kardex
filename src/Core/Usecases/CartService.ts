import CartItem from '@Core/Domain/Entities/CartItem';
import { Movements } from '@Core/Domain/Entities/Movements';
import  Cart  from '@Entities/Cart';
import { CartRepository } from '@Repositories/Cart/CartRepository';
import { MovementsService } from './MovementsService';

interface ICartService {
    addProductToCart(uid: string, data: CartItem): Promise<Cart>
    getCart: (uid: string) => Promise<Cart>
    removeProduct: (uid: string, idCartItem: string)=> Promise<Cart>
    editQuantityOfCartItem: (uid: string, idCartItem: string, quantity:number)=> Promise<Cart>
    buyProducts: (uid: string, cart: Cart)=> Promise<Cart>
}

export class CartService implements ICartService {
    cartRepository: CartRepository
    movementService: MovementsService
    constructor(pr: CartRepository, mr:MovementsService) {
        this.cartRepository = pr
        this.movementService = mr
    }

    async getCart (uid: string): Promise<Cart> {
        return this.cartRepository.getCart(uid)
    }

    async addProductToCart (uid: string, carItem: CartItem): Promise<Cart> {
        const cartCurrent = await this.cartRepository.getCart(uid);
        const newStateCar = cartCurrent.addItem(carItem)
        return this.cartRepository.updateProductsToCart(uid, newStateCar)
    }
    async removeProduct (uid: string, idCartItem: string): Promise<Cart> {
        const cartCurrent = await this.cartRepository.getCart(uid);
        const newStateCar = cartCurrent.removeItem(idCartItem)
        return this.cartRepository.removeProductsToCart(uid, newStateCar)
    }

    async editQuantityOfCartItem (uid: string, idCartItem: string, quantity:number): Promise<Cart> {
        const cartCurrent = await this.cartRepository.getCart(uid);
        const newStateCar = cartCurrent.editItem(idCartItem,quantity)
        return this.cartRepository.updateProductsToCart(uid, newStateCar)
    }

    async buyProducts (uid: string, cart: Cart):  Promise<Cart> {
        let carState = cart;
        for await (const item of cart.items) {
            const data= { type:"Venta", name:item.title, cant:item.quantity, idProduct:item.id}
            await this.movementService.addMovements(new Movements(data))
            const newCart = await this.removeProduct(uid,item.id)
            carState = await this.cartRepository.removeProductsToCart(uid,newCart)
        }
    
        return carState
    }
}