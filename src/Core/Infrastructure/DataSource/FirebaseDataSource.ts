import { getDatabase, ref, get, update, remove,push,set,query,orderByChild,equalTo } from "firebase/database";
import { app,auth } from "firebaseConfig";

const database = getDatabase(app);
export class FirebaseDataSource {
  private static instance: FirebaseDataSource

  static getInstance():FirebaseDataSource {
    if (!FirebaseDataSource.instance) FirebaseDataSource.instance = new FirebaseDataSource()
    return FirebaseDataSource.instance
  }

  
  async get<T>(uri: string): Promise<T | undefined> {
    const refDb = ref(database,`${uri}`);
    const resp = await get(refDb)
    return resp.val()
  }

  async getByProperty<T>(uri: string,  property: string, value: any): Promise<T | undefined> {
    const refDb = ref(database,`${uri}`);
    const queryRef = query(refDb, orderByChild(property),equalTo(value));
    const resp = await get(queryRef);
    return resp.val()
  }


  async post<T>(uri: string, data: any): Promise<void> {
    const postListRef = await ref(database, `${uri}`);
    const newPostRef = await push(postListRef);
    const resp = await  set(newPostRef, data);
    return resp
  }

  async patch<T>(uri: string, data: any): Promise<T | void > {
    const resp = await update(ref(database, `${uri}`), data);
    return resp
  }

  async put<T>(uri: string, data: any): Promise<T | void> {
    const resp = await update(ref(database, `${uri}`), data);
    return resp
  }

  async delete<T>(uri: string): Promise<T | void> {
    const resp = await remove(ref(database, `${uri}`));
    return resp
  }
 
  async login(email:string, password:string): Promise<any> {
    return auth.signInWithEmailAndPassword(email, password)
  }

  async logout(): Promise<void> {
    return auth.signOut()
  }


  async signup(email:string, password:string,nameUser: string, isEmploye: boolean): Promise<void> {
    return await auth.createUserWithEmailAndPassword(email, password).then(async()=>{
      const data={ id:auth.currentUser?.uid, email:auth.currentUser?.email, name:nameUser, isEmploye:isEmploye}
      await this.post(`/users`,data)
    })
  }

}
