import { Product } from "@Entities/Product";

export interface ProductRepository {
    getProducts: ()=> Promise<Product[]>
    addProduct(data: Product): Promise<Product | undefined>
    getProductById(id: string): Promise<Product>
    updateProductById(id: string, stock: number): Promise<Product | undefined> 
}