import { FirebaseDataSource } from '@Core/Infrastructure/DataSource/FirebaseDataSource';
import { ProductRepository } from '@Repositories/Product/ProductsRepository';
import { Product } from '../../../Domain/Entities/Product';
import { ProductMapper } from '../../../Mappers/ProductMapper';

export class ProductRepositoryImpl implements ProductRepository {
	mapper: ProductMapper;

	constructor(mapper: ProductMapper) {
		this.mapper = mapper;
	}

	async getProducts(): Promise<Product[]> {
		const fireDataSource = new FirebaseDataSource();
		const response = await fireDataSource.get<any>('/products');
		const repsonseArray = !!response ? Array.from(Object.values(response)) : [];
		const mapperProducts = repsonseArray?.map((prodDb) => this.mapper.PersistenceToDomain(prodDb));
		return mapperProducts;
	}

	async addProduct(data: Product): Promise<Product | undefined> {
		const fireDataSource = new FirebaseDataSource();
		await fireDataSource.post('/products', data);
		return data;
	}

	async getProductById(id: string): Promise<Product> {
		const fireDataSource = new FirebaseDataSource();
		const respProduct = await fireDataSource.getByProperty<Product | any>('/products/', '_id', id);
		return this.mapper.PersistenceToDomain(Object.values(respProduct)[0]);
	}

	async updateProductById(id: string, stock: number): Promise<Product | undefined> {
		const fireDataSource = new FirebaseDataSource();
		const respProduct = await fireDataSource.getByProperty<Product | any>('/products/', '_id', id);
		const responseKey = Object.keys(respProduct)[0];
		await fireDataSource.patch(`/products/${responseKey}`, { _stock: stock });
		return undefined;
	}
}
