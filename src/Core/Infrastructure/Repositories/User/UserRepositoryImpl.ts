import { UserRepository } from "./UserRepository";
import { FirebaseDataSource } from '../../DataSource/FirebaseDataSource';
import { UserMapper } from "@Core/Mappers/UserMapper";
import { Either, right, left} from '../../../shared/Either';
import { pipe } from "fp-ts/function";
import * as O from "fp-ts/Option";
import { User } from '@Domain/Entities/User';
import { Employes } from '@Domain/Entities/Employes';
export class UserRepositoryImpl implements UserRepository {
  mapper: UserMapper

  constructor (mapper: UserMapper) {
    this.mapper= mapper
  }
  async signup(email:string, password:string,name: string, isEmploye: boolean): Promise<void> {
    const fireDataSource = new FirebaseDataSource()
    return  fireDataSource.signup(email, password, name, isEmploye)
  }

  async login(email:string, password:string): Promise<void> {
    const fireDataSource = new FirebaseDataSource()
    return  fireDataSource.login(email, password)
  }
  
  async logout(): Promise<void> {
    const fireDataSource = new FirebaseDataSource()
    return  fireDataSource.logout()
  }
  
  async greeting(uid: string): Promise<Either<string, User | Employes>>{
    const fireDataSource = new FirebaseDataSource()
    const userFB= await fireDataSource.getByProperty("users","id",uid)
    const isNone =pipe(userFB,O.fromNullable,O.isNone)
    if(isNone){
      return left("error in the message")
    }
    const userMapper = this.mapper.PersistenceToDomain(userFB)
    return right(userMapper);
  }
}
