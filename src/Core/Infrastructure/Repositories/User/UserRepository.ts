import { Either } from '../../../shared/Either';
import { User } from '@Domain/Entities/User';
import { Employes } from '@Domain/Entities/Employes';

export interface UserRepository {
  signup(email:string, password:string,name: string, isEmploye: boolean): Promise<void | any>
  logout(): Promise<void | any>
  login(email:string, password:string): Promise<void | any>
  greeting(uid:string): Promise<Either<string, User | Employes>>
}
