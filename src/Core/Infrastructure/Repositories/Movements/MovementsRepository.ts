import { Movements } from "@Entities/Movements";

export interface MovementsRepository {
    getMovements: ()=> Promise<Movements[]>
    addMovement(data: Movements): Promise<Movements | undefined>
}