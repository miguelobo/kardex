import { FirebaseDataSource } from '@Core/Infrastructure/DataSource/FirebaseDataSource';
import { MovementsRepository } from '@Repositories/Movements/MovementsRepository';
import { Movements } from '@Core/Domain/Entities/Movements';
import { MovementsMapper } from '../../../Mappers/MovementsMapper';
export class MovementsRepositoryImpl implements MovementsRepository {
	mapper: MovementsMapper;

	constructor(mapper: MovementsMapper) {
		this.mapper = mapper;
	}

	async getMovements(): Promise<Movements[]> {
		const fireDataSource = new FirebaseDataSource();
		const response = await fireDataSource.get<any>('/movements');
		const repsonseArray = !!response ? Array.from(Object.values(response)) : [];
		const mapperMovements = repsonseArray?.map((prodDb) => this.mapper.PersistenceToDomain(prodDb));
		return mapperMovements;
	}

	async addMovement(data: Movements): Promise<Movements | undefined> {
		const fireDataSource = new FirebaseDataSource();
		await fireDataSource.post('/movements', data);
		return data;
	}
}
