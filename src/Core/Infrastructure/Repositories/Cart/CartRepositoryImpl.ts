import { FirebaseDataSource } from "@Core/Infrastructure/DataSource/FirebaseDataSource";
import { CartRepository } from "@Repositories/Cart/CartRepository";
import Cart from '@Core/Domain/Entities/Cart';
import { CartMapper } from '../../../Mappers/CartMapper';

export class CartRepositoryImpl implements CartRepository {
    mapper: CartMapper; 

    constructor (mapper: CartMapper) {
        this.mapper= mapper
    }

    async getCart(uid: string): Promise<Cart>{
        const fireDataSource = new FirebaseDataSource()
        const response = await fireDataSource.get<any >(`/carts/${uid}/items`)
        return this.mapper.PersistenceToDomain(response)
    }

    async updateProductsToCart(uid: string, data: Cart): Promise<Cart> {
        const fireDataSource = new FirebaseDataSource()
        await fireDataSource.patch<any >(`/carts/${uid}/items`,{ ...data.items })
        return data
    }

    async removeProductsToCart(uid: string, data: Cart): Promise<Cart> {
        const fireDataSource = new FirebaseDataSource()
        await fireDataSource.delete<any >(`/carts/${uid}/items`)
        await fireDataSource.patch<any >(`/carts/${uid}/items`,{ ...data.items })
        return data
    }
    
}



