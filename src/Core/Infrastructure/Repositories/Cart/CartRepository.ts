import Cart from "@Entities/Cart";

export interface CartRepository {
    updateProductsToCart(uid: string, data: Cart): Promise<Cart>
    removeProductsToCart(uid: string, data: Cart): Promise<Cart>
    getCart: (uid: string) => Promise<Cart>
}