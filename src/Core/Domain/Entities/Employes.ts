import { User, IUserProps } from './User';

export class Employes extends User {
   
    constructor({id, name, email, isEmploye}: IUserProps) {
      super({id,name,email, isEmploye})
    }
  
    greeting(): string {
      return "Empleado: Bienvenido a Kardex "+ this._name;
    }

    static create(props: IUserProps): Employes {
      return new Employes(props)
    }
}
