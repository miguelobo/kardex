import { Entity } from '../Entity';

interface MovementsProps {
  readonly id?: string
  readonly type: string
  readonly name: string
  readonly cant: number
  readonly idProduct: string
}

export class Movements extends Entity implements MovementsProps {

  private readonly _type: string
  private readonly _name: string
  private readonly _cant: number
  private readonly _idProduct: string

  constructor({id, type,  name, cant,idProduct}: MovementsProps) {
    super(id)
    this._type = type
    this._name = name
    this._cant = cant
    this._idProduct = idProduct
  }

  get type(): string {
    return this._type;
  }

  get name(): string {
    return this._name;
  }

  get cant(): number {
    return this._cant;
  }
  get idProduct(): string {
    return this._idProduct;
  }

  static create(props: MovementsProps): Movements {
    return new Movements(props)
  }

}
