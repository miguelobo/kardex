import { Entity } from "../Entity";
export interface IUserProps {
  readonly id?: string
  readonly name: string
  readonly email: string
  readonly isEmploye: boolean
}
export class User extends Entity {
  _name: string;
  _email: string;
  _isEmploye: boolean;

  constructor({id, name, email, isEmploye}: IUserProps) {
    super(id)
    this._name = name;
    this._email = email;
    this._isEmploye = isEmploye;
  }

  greeting(): string {
    return "Usuario: Bienvenido a Kardex "+ this._name;
  }

  static create(props: IUserProps): User {
    return new User(props)
  }

}

  



