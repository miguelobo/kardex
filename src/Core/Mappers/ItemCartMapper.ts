import {Mapper} from "./Mapper";
import CartItem from '../Domain/Entities/CartItem';
import { Product } from '../Domain/Entities/Product';


export class ItemCartMapper extends Mapper<CartItem> {

  DTOtoDomain(rowCart: Product): CartItem {
    return {
      id: rowCart.id,
      image:"http://x.annihil.us/u/prod/marvel/i/mg/3/40/4bb4680432f73/standard_fantastic.jpg",
      price: rowCart.price,
      quantity:1,
      title: rowCart.name
    }
  }
  
    PersistenceToDomain(rawDB: any): CartItem {
      return { 
      id: rawDB.id,
      image:"http://x.annihil.us/u/prod/marvel/i/mg/3/40/4bb4680432f73/standard_fantastic.jpg",
      price: rawDB.price,
      quantity:rawDB.quantity ,
      title: rawDB.name
    }
  }
}
