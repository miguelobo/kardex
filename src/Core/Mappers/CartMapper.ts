import {Mapper} from "./Mapper";
import Cart from "../Domain/Entities/Cart";
import CartItem from '../Domain/Entities/CartItem';

export interface IDTOCart {
  readonly id?: string
  readonly items: CartItem[]
}

export class CartMapper extends Mapper<Cart> {

  DTOtoDomain(rowCart: IDTOCart): Cart {
    return Cart.create( rowCart.items )
  }

  PersistenceToDomain(rawDB: any): Cart {
    const itemsDB: any[] = Object.values(rawDB??{}) || []
    return Cart.create( itemsDB??[])
  }

}
