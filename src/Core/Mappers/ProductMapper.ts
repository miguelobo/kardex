import {Mapper} from "./Mapper";
import {Product} from "../Domain/Entities/Product";

export interface IDTOProduct {
  readonly id?: string
  readonly name: string
  readonly description: string
  readonly price: number
  readonly code: string
  readonly stock: number
}

export class ProductMapper extends Mapper<Product> {

  DTOtoDomain(rowProduct: IDTOProduct): Product {
    return Product.create({
      id: rowProduct?.id,
      name: rowProduct.name,
      description: rowProduct.description,
      price: rowProduct.price,
      code: rowProduct.code,
      stock: rowProduct.stock,
    })
  }

  PersistenceToDomain(rawDB: any): Product {
    return Product.create({
      id:rawDB?._id,
      name: rawDB._name,
      description: rawDB._description,
      price: rawDB._price,
      code: rawDB._code,
      stock: rawDB._stock,
    })
  }

}
