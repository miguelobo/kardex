export abstract class Mapper<T> {
  abstract DTOtoDomain(raw: any): T
  //abstract DomainToDTO(t?: T): any
  //abstract DomainToPersistence(t: T): any
  abstract PersistenceToDomain(raw: any): T
}
