import {Mapper} from "./Mapper";
import { User } from '../Domain/Entities/User';
import { Employes } from '../Domain/Entities/Employes';

export interface IDTOUser {
  readonly id?: string
  readonly name: string
  readonly email: string
  readonly isEmploye: boolean
}

export class UserMapper extends Mapper<User> {

  DTOtoDomain(rowUser: IDTOUser): User {
    return User.create({
      id: rowUser.id,
      name: rowUser.name,
      email: rowUser.email,
      isEmploye: rowUser.isEmploye,
    })
  }

  PersistenceToDomain(rawDB: any): User | Employes {
    const valuesDB: any = Object.values(rawDB)[0]
    const key = Object.keys(rawDB)[0]
    const TypeUser = !!valuesDB.isEmploye? Employes : User
    return TypeUser.create({
      id:valuesDB.name??key,
      name: valuesDB.name,
      email: valuesDB.email,
      isEmploye: valuesDB.isEmploye,
    })
  }

}
