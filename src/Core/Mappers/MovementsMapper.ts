import {Mapper} from "./Mapper";
import {Movements} from "../Domain/Entities/Movements";

export interface IDTOMovements {
  readonly id?: string
  readonly type: string
  readonly name: string
  readonly cant: number
  readonly idProduct: string
}

export class MovementsMapper extends Mapper<Movements> {

  DTOtoDomain(rowMovements: IDTOMovements): Movements {
    return Movements.create({
      type:rowMovements.type,
      name:rowMovements.name,
      cant:rowMovements.cant,
      idProduct:rowMovements.idProduct,
    })
  }

  PersistenceToDomain(rawDB: any): Movements {
    return Movements.create({
      id:rawDB?._id,
      type:rawDB._type,
      name:rawDB._name,
      cant:rawDB._cant,
      idProduct:rawDB._idProduct
    })
  }

}
