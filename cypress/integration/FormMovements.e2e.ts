describe('Form Movements Test', () => {
	beforeEach(() => {
		cy.login();
		cy.visit('http://localhost:3000/movements');
	});

	it('should be able to visit page Movements and have form Correct', () => {
		cy.get('#addMovements').click();
        cy.get('body').click();
        cy.get('#mui-component-select-idProduct')
        cy.get('.MuiGrid-root:nth-child(4) .MuiInputBase-input').type('5');
	});

	 it('should be able to visit page Movements and have form Incorrect', () => {
	 	cy.get('#addMovements').click();
        cy.get('body').click();
        cy.get('#mui-component-select-idProduct')
        cy.get('.MuiGrid-root:nth-child(4) .MuiInputBase-input').type('-5');
        cy.get('form').submit();
        cy.contains('Campo requerido')
         cy.contains('Numero Positivo')
	 });

});

